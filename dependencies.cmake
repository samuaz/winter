#
# Created by AZCONA VARGAS, SAMUEL EDUARDO
#

############################
##     EXTERNAL LIBS      ##
############################
include(${PROJECT_SOURCE_DIR}/cmake/dep_protobuf.cmake)
include(${PROJECT_SOURCE_DIR}/cmake/dep_grpc.cmake)
include(${PROJECT_SOURCE_DIR}/cmake/dep_openssl.cmake)
include(${PROJECT_SOURCE_DIR}/cmake/dep_gtest.cmake)
include(${PROJECT_SOURCE_DIR}/cmake/dep_json.cmake)
include(${PROJECT_SOURCE_DIR}/cmake/dep_jwt.cmake)
include(${PROJECT_SOURCE_DIR}/cmake/dep_mysql.cmake)
include(${PROJECT_SOURCE_DIR}/cmake/dep_redis.cmake)
include(${PROJECT_SOURCE_DIR}/cmake/dep_yaml.cmake)
include(${PROJECT_SOURCE_DIR}/cmake/util_grpc_proto.cmake)
