#
# Created by AZCONA VARGAS, SAMUEL EDUARDO
#

###############################
## CONFIG BUILD SYSTEM HOST ##
###############################
if (UNIX AND NOT APPLE)
    set(HOST_OS "linux")
    set(HOST_OS_2 "linux")
    set(MYSQL_PLATFORM linux-glibc2.12)
endif ()

if (APPLE)
    set(CMAKE_MACOSX_RPATH 1)
    set(HOST_OS "osx")
    set(HOST_OS_2 "macos")
    set(MYSQL_PLATFORM macos10.14)
endif ()


############################
##     EXTERNAL LIBS      ##
############################

#include(ExternalProject)
include(FetchContent)
set(FETCHCONTENT_QUIET OFF)
set(THIRD_PARTY_DIR ${CMAKE_SOURCE_DIR}/third_party)

## openssl

FetchContent_Declare(
        third_party/openssl
        GIT_REPOSITORY https://github.com/openssl/openssl.git
        GIT_TAG        OpenSSL_1_1_1-stable
        GIT_PROGRESS   TRUE
        CONFIGURE_COMMAND ./config --prefix=${THIRD_PARTY_DIR}/openssl
        INSTALL_COMMAND make install_sw
        SOURCE_DIR ${THIRD_PARTY_DIR}/openssl
)
set(OPENSSL_ROOT_DIR ${openssl_SOURCE_DIR} CACHE INTERNAL "")
set(OPENSSL_CRYPTO_LIBRARY ${openssl_SOURCE_DIR}/crypto CACHE INTERNAL "")
set(OPENSSL_INCLUDE_DIR ${openssl_SOURCE_DIR}/include CACHE INTERNAL "")
link_directories("${openssl_SOURCE_DIR}/lib")

#[[ExternalProject_add(
        openssl
        GIT_REPOSITORY https://github.com/openssl/openssl.git
        GIT_TAG        OpenSSL_1_1_1-stable
        GIT_PROGRESS   TRUE
        CONFIGURE_COMMAND ./config --prefix=${CMAKE_CURRENT_BINARY_DIR}/openssl
        SOURCE_DIR ${THIRD_PARTY_DIR}/openssl
        BUILD_COMMAND make depend && make
        INSTALL_COMMAND make install_sw
        BUILD_IN_SOURCE 1
)

# The first external project will be built at *configure stage*
execute_process(
        COMMAND ${CMAKE_COMMAND} --build . ${CMAKE_CURRENT_BINARY_DIR}/openssl
        WORKING_DIRECTORY  ${CMAKE_CURRENT_BINARY_DIR}/openssl
        RESULT_VARIABLE openssl_install_result
        OUTPUT_VARIABLE openssl_OUTPUT_VARIABLE)
MESSAGE(STATUS "OPENSSL_CMD_ERROR:" ${openssl_install_result})
MESSAGE(STATUS "OPENSSL_CMD_OUTPUT:" ${openssl_OUTPUT_VARIABLE})
set(OPENSSL_ROOT_DIR ${CMAKE_CURRENT_BINARY_DIR}/openssl CACHE INTERNAL "")
set(OPENSSL_INCLUDE_DIR "${CMAKE_CURRENT_BINARY_DIR}/openssl/include")
link_directories("${CMAKE_CURRENT_BINARY_DIR}/openssl/libs")]]


#set(OPENSSL_ROOT_DIR ${openssl_SOURCE_DIR} CACHE INTERNAL "")
#set(OPENSSL_CRYPTO_LIBRARY ${openssl_SOURCE_DIR}/crypto CACHE INTERNAL "")
#set(OPENSSL_INCLUDE_DIR ${openssl_SOURCE_DIR}/include CACHE INTERNAL "")

## END openssl

## protobuf ##
## we use the prebuild proto compiler binary to use to build our proto files
FetchContent_Declare(
        third_party/protobuf
        GIT_REPOSITORY https://github.com/protocolbuffers/protobuf.git
        GIT_TAG        v3.14.0
        SOURCE_SUBDIR  cmake
        SOURCE_DIR ${THIRD_PARTY_DIR}/protobuf
)

FetchContent_Declare(
        third_party/protobuf_compiler
        URL https://github.com/protocolbuffers/protobuf/releases/download/v3.14.0/protoc-3.14.0-${HOST_OS}-x86_64.zip
        SOURCE_DIR ${THIRD_PARTY_DIR}/protobuf_compiler
)
set(protobuf_BUILD_TESTS OFF)
## END protobuf ##

## grpc ##
FetchContent_Declare(
        third_party/grpc
        GIT_REPOSITORY https://github.com/grpc/grpc.git
        GIT_TAG        v1.34.0
        GIT_PROGRESS   TRUE
        SOURCE_DIR ${THIRD_PARTY_DIR}/grpc
)

# this is the plugin for protobuf needed to generate the grpc services
# to get this you need to go https://packages.grpc.io/ and search the commit tag number of the release of grpc that are you using for example for v1.33.x is 33fa8858864e21b996b1c33cc0766955da8880c6-15ffb170-28db-4c44-b255-104ac78f6e41
FetchContent_Declare(
        third_party/grpc_plugin
        URL https://packages.grpc.io/archive/2020/12/d7b70c3ea25c48ffdae7b8bd3c757594d4fff4b6-2be69c7e-9b25-4273-a7d4-3840da2d6723/protoc/grpc-protoc_${HOST_OS_2}_x64-1.35.0-dev.tar.gz
        SOURCE_DIR ${THIRD_PARTY_DIR}/grpc_plugin
)
## END grpc ##

## yaml_cpp
FetchContent_Declare(
        third_party/yaml_cpp
        GIT_REPOSITORY https://github.com/jbeder/yaml-cpp.git
        GIT_TAG        master
        GIT_PROGRESS   TRUE
        SOURCE_DIR ${THIRD_PARTY_DIR}/yaml_cpp
)
## END yaml_cpp

## cpp_redis
FetchContent_Declare(
        third_party/cpp_redis
        GIT_REPOSITORY https://github.com/cpp-redis/cpp_redis.git
        GIT_TAG        master
        GIT_PROGRESS   TRUE
        SOURCE_DIR ${THIRD_PARTY_DIR}/cpp_redis
)
## END cpp_redis

## mysql_client
set(MYSQL_VERSION 8.0.22)
set(MYSQL_CONNECTOR_EXTENSION tar.gz)
set(MYSQL_EXTENSION tar.xz)
if (UNIX AND NOT APPLE)
    set(MYSQL_PLATFORM linux-glibc2.12)
endif ()
if (APPLE)
    set(MYSQL_PLATFORM macos10.14)
endif ()

FetchContent_Declare(
        third_party/mysql_client
        URL https://dev.mysql.com/get/Downloads/MySQL-8.0/mysql-${MYSQL_VERSION}-${MYSQL_PLATFORM}-x86_64.${MYSQL_EXTENSION}
        SOURCE_DIR ${THIRD_PARTY_DIR}/mysql_client
)

FetchContent_Declare(
        third_party/boost
        URL https://dl.bintray.com/boostorg/release/1.75.0/source/boost_1_75_0.tar.gz
        SOURCE_DIR ${THIRD_PARTY_DIR}/boost
)

#set(BOOST_ENABLE_CMAKE ON)
#FetchContent_Declare(
#        boost
#        GIT_REPOSITORY https://github.com/boostorg/boost.git
#        GIT_PROGRESS   TRUE
#        SOURCE_DIR ${THIRD_PARTY_DIR}/boost
#)


set(WITH_BOOST ${THIRD_PARTY_DIR}/boost CACHE INTERNAL "")
set(WITH_JDBC TRUE CACHE INTERNAL "")
set(BUILD_STATIC ON CACHE INTERNAL "")
set(WITH_SSL=/usr/local)
#set(CMAKE_INSTALL_LIBDIR ${THIRD_PARTY_DIR})
#set(CMAKE_INSTALL_INCLUDEDIR ${THIRD_PARTY_DIR})
#set(CMAKE_INSTALL_DOCDIR ${THIRD_PARTY_DIR}
set(INSTALL_INCLUDE_DIR ${THIRD_PARTY_DIR} CACHE INTERNAL "")  # Forces the value
set(INSTALL_LIB_DIR ${THIRD_PARTY_DIR} CACHE INTERNAL "")  # Forces the value
set(INSTALL_DOC_DIR ${THIRD_PARTY_DIR} CACHE INTERNAL "")  # Forces the value
set(MYSQL_LIB_DIR ${THIRD_PARTY_DIR}/mysql_client/lib)
set(MYSQL_INCLUDE_DIR ${THIRD_PARTY_DIR}/mysql_client/include)

#FetchContent_Declare(
#        mysql_connector_cpp
#        GIT_REPOSITORY https://github.com/mysql/mysql-connector-cpp.git
#        GIT_TAG        8.0
#        GIT_PROGRESS   TRUE
#        SOURCE_DIR ${THIRD_PARTY_DIR}/mysql_connector_cpp
#)

FetchContent_Declare(
        third_party/mysql_connector_cpp
        URL https://dev.mysql.com/get/Downloads/Connector-C++/mysql-connector-c++-${MYSQL_VERSION}-${MYSQL_PLATFORM}-x86-64bit.${MYSQL_CONNECTOR_EXTENSION}
        SOURCE_DIR ${THIRD_PARTY_DIR}/mysql_connector_cpp
)

## END mysql_connector_cpp

## JWT
set(CPP_JWT_USE_VENDORED_NLOHMANN_JSON ON CACHE INTERNAL "")
set(CPP_JWT_BUILD_TESTS FALSE CACHE INTERNAL "")
set(CPP_JWT_BUILD_EXAMPLES FALSE CACHE INTERNAL "")
FetchContent_Declare(
        third_party/jwt
        GIT_REPOSITORY https://github.com/arun11299/cpp-jwt.git
        GIT_TAG master
        SOURCE_DIR ${THIRD_PARTY_DIR}/jwt
)
#FetchContent_GetProperties(jwt)
#if(NOT jwt_POPULATED)
#    FetchContent_Populate(jwt)
#endif()
## END JWT

FetchContent_Declare(
        third_party/json
        GIT_REPOSITORY https://github.com/nlohmann/json.git
        GIT_TAG master
        SOURCE_DIR ${THIRD_PARTY_DIR}/json
)

## googletest ##
FetchContent_Declare(
        third_party/googletest
        GIT_REPOSITORY https://github.com/google/googletest.git
        GIT_TAG        release-1.10.0
        SOURCE_DIR ${THIRD_PARTY_DIR}/googletest
)
## END googletest##

## make all libs available
#FetchContent_MakeAvailable(protobuf protobuf_compiler grpc_plugin grpc boost mysql_client mysql_connector_cpp yaml_cpp cpp_redis openssl jwt json googletest)
FetchContent_MakeAvailable(third_party/protobuf third_party/protobuf_compiler third_party/grpc_plugin third_party/boost third_party/mysql_client third_party/mysql_connector_cpp third_party/openssl)

FetchContent_GetProperties(third_party/grpc)
if(NOT grpc_POPULATED)
    FetchContent_Populate(third_party/grpc)
    add_subdirectory(${grpc_SOURCE_DIR} ${grpc_BINARY_DIR} EXCLUDE_FROM_ALL)
endif()

FetchContent_GetProperties(third_party/yaml_cpp)
if(NOT yaml_cpp_POPULATED)
    FetchContent_Populate(third_party/yaml_cpp)
    add_subdirectory(${yaml_cpp_SOURCE_DIR} ${yaml_cpp_BINARY_DIR} EXCLUDE_FROM_ALL)
endif()

FetchContent_GetProperties(third_party/cpp_redis)
if(NOT cpp_redis_POPULATED)
    FetchContent_Populate(third_party/cpp_redis)
    add_subdirectory(${cpp_redis_SOURCE_DIR} ${cpp_redis_BINARY_DIR} EXCLUDE_FROM_ALL)
endif()

FetchContent_GetProperties(third_party/jwt)
if(NOT jwt_POPULATED)
    FetchContent_Populate(third_party/jwt)
    add_subdirectory(${jwt_SOURCE_DIR} ${jwt_BINARY_DIR} EXCLUDE_FROM_ALL)
endif()

FetchContent_GetProperties(third_party/json)
if(NOT json_POPULATED)
    FetchContent_Populate(third_party/json)
    add_subdirectory(${json_SOURCE_DIR} ${json_BINARY_DIR} EXCLUDE_FROM_ALL)
endif()

FetchContent_GetProperties(third_party/googletest)
if(NOT googletest_POPULATED)
    FetchContent_Populate(third_party/googletest)
    add_subdirectory(${googletest_SOURCE_DIR} ${googletest_BINARY_DIR} EXCLUDE_FROM_ALL)
endif()

execute_process(
        COMMAND ./config --prefix=${openssl_SOURCE_DIR}
        WORKING_DIRECTORY ${openssl_SOURCE_DIR}
        RESULT_VARIABLE openssl_install_result
        OUTPUT_VARIABLE openssl_OUTPUT_VARIABLE)
execute_process(
        COMMAND make install_sw
        WORKING_DIRECTORY ${openssl_SOURCE_DIR}
        RESULT_VARIABLE openssl_install_result
        OUTPUT_VARIABLE openssl_OUTPUT_VARIABLE)
MESSAGE(STATUS "OPENSSL_CMD_ERROR:" ${openssl_install_result})
MESSAGE(STATUS "OPENSSL_CMD_OUTPUT:" ${openssl_OUTPUT_VARIABLE})
include_directories(/usr/include)
include_directories(/usr/local/include)
include_directories(${openssl_SOURCE_DIR}/include)
#include_directories(${CMAKE_CURRENT_BINARY_DIR}/openssl/include)
link_directories(${CMAKE_CURRENT_BINARY_DIR}/openssl/lib)
include_directories(${json_SOURCE_DIR}/include/)
include_directories(${jwt_SOURCE_DIR}/include/)
include_directories(${cpp_redis_SOURCE_DIR}/tacopie/includes)
include_directories(${cpp_redis_SOURCE_DIR}/includes)
include_directories(${yaml_cpp_SOURCE_DIR}/include)
include_directories(${mysql_connector_cpp_SOURCE_DIR}/include)
include_directories(${THIRD_PARTY_DIR}/boost/)
link_directories(${mysql_connector_cpp_SOURCE_DIR}/lib64)
set(LIBS protobuf::libprotobuf third_party/grpc grpc++ mysqlcppconn8-static mysqlcppconn-static yaml-cpp third_party/cpp_redis tacopie)
